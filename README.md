## Longevity InTime - Frontend Sample Code for Job Application ##

Applicant / Coder Name: Mark Danilak

Task Chosen: Task C - "Patient Connector"

Hello! For submitting my sample code to Longevity InTime in order to apply for the FrontEnd Developer position, I utilized the programming languages Flutter and Dart in order to create my application.

A few quick comments:
- Flutter, Dart, Android Studio, and potentially other tools are required to run and use the program. 
- My application is not made to fit any screen. Therefore, it is preferred to have it running in a large screen. 
- Some colors, text, icons, images, etc. may be different, as I had to utilize what I had on my end. 
- Some layouts and functionality might differ only slightly due to not knowing all possible solutions to tackling these problems in Flutter and Dart. 
- I added an extra button to the bottom named "Next Page" to show my ability to traverse different screens in a future application, but initially also because I planned on doing something else on a different screen as well. 
- I added two value displays at the bottom of the application to show the "save" ability of the user entering data and it being saved for future use, as it is vital to this screen's functionality in the app. 
- The list of diseases displayed is lower than per the provided reference, as I was running into errors when trying to resolve it and I wasn't able to figure it out before submitting my work here.

Thank you again so much for giving me the opportunity to provide a sample program to you! I hope you are pleased with the work you see and I hope to join the team so I can continue creating amazing and important work for Longevity InTime, and the people who use it! :)

I wish you all the best and a pleasant rest of your day!
